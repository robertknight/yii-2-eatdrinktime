<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property integer $item_id
 * @property string $file
 *
 * @property Item $item
 */
class Image extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'image';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['file'], 'required'],
			[['file'], 'image',
				'maxSize' => 102400,
				'extensions' => ['gif', 'jpg', 'jpeg', 'png'],
				'minHeight' => 400,
				'minWidth' => 400,
				'tooBig' => 'The file "{file}" is too big. Its size cannot exceed 100kb.',
				'on' => ['guestSubmit', 'update']
			],
			[['file'], 'image',
				'maxSize' => 102400,
				'extensions' => ['gif', 'jpg', 'jpeg', 'png'],
				'minHeight' => 400,
				'minWidth' => 400,
				'maxFiles' => 10,
				'tooBig' => 'The file "{file}" is too big. Its size cannot exceed 100kb.',
				'on' => 'adminSubmit'
			]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'item_id' => 'Item ID',
			'file' => 'File',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getItem()
	{
		return $this->hasOne(Item::className(), ['id' => 'item_id']);
	}
}
