<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Vote extends Model
{
	public $times = [];

	public function rules()
	{
		return [
			[['times'], 'safe']
		];
	}

	public function attributeLabels()
	{
		return ['times' => 'Time'];
	}
}
