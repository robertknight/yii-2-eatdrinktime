<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ListItem extends Model
{
	public $id;

	public function rules()
	{
		return [
			[['id'], 'safe']
		];
	}

	public function attributeLabels()
	{
		return ['id' => 'Item'];
	}
}
