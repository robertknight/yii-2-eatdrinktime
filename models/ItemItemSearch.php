<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ItemItem;

/**
 * ItemItemSearch represents the model behind the search form about `app\models\ItemItem`.
 */
class ItemItemSearch extends ItemItem
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id1', 'id2'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = ItemItem::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id1' => $this->id1,
			'id2' => $this->id2,
		]);

		return $dataProvider;
	}
}
