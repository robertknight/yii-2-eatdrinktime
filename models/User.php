<?php

namespace app\models;

use yii\base\Object;
use yii\web\IdentityInterface;

class User extends Object implements IdentityInterface
{
	public $id;
	public $username;
	public $password;

	private static $users = [
		'100' => [
			'id' => '100',
			'username' => 'robert',
			'password' => 'robert'
		]
	];

	public static function findIdentity($id)
	{
		return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
	}

	public static function findByUsername($username)
	{
		foreach (self::$users as $user)
			if (strcasecmp($user['username'], $username) === 0)
				return new static($user);

		return null;
	}

	public static function findIdentityByAccessToken($token, $type = null) {}

	public function getId()
	{
		return $this->id;
	}

	public function getAuthKey() {}

	public function validateAuthKey($authKey) {}

	public function validatePassword($password)
	{
		return $this->password === $password;
	}
}
