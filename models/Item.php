<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "item".
 *
 * @property integer $id
 * @property string $title
 * @property integer $type
 * @property integer $morning
 * @property integer $afternoon
 * @property integer $night
 * @property integer $anytime
 * @property string $timestamp
 *
 * @property Image[] $images
 * @property ItemItems[] $itemItems
 */
class Item extends ActiveRecord
{
	public $times = [];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'item';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'type'], 'required'],
			[['title'], 'trim'],
			[['title'], 'string', 'max' => 70],
			[['title'], 'unique'],
			[['title'], 'caseInsensitiveUnique', 'except' => ['update']],
			[['type'], 'integer'],
			[['type'], 'in', 'range' => [0, 1]],
			[['times'], 'safe']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Title',
			'type' => 'Type',
			'morning' => 'Morning',
			'afternoon' => 'Afternoon',
			'night' => 'Night',
			'anytime' => 'Anytime',
			'timestamp' => 'Timestamp',
			'times' => 'Time'
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getImages()
	{
		return $this->hasMany(Image::className(), ['item_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getItemItems()
	{
		return $this->hasMany(ItemItem::className(), ['id2' => 'id']);
	}

	public function caseInsensitiveUnique($attribute)
	{
		$query = (new \yii\db\Query)
			->select(['id'])
			->from(Item::tableName())
			->where('lower(title)=:title', [':title' => strtolower($this->title)])
			->exists();

		if ($query)
			$this->addError($attribute, 'Title "'.$this->title.'" has already been taken.');
	}
}
