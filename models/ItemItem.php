<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Item;

/**
 * This is the model class for table "item_item".
 *
 * @property integer $id1
 * @property integer $id2
 *
 * @property Item $id10
 * @property Item $id20
 */
class ItemItem extends ActiveRecord
{
	public $links = [];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'item_item';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			// [['id1', 'id2'], 'required'],
			// [['id1', 'id2'], 'integer'],
			// [['id1'], 'compare', 'operator' => '<', 'compareAttribute' => $this->id2],
			// [['id1', 'id2'], 'unique', 'targetAttribute' => ['id1', 'id2']],
			[['links'], 'safe']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id1' => 'Id1',
			'id2' => 'Id2',
			'links' => 'Link'
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getId10()
	{
		return $this->hasOne(Item::className(), ['id' => 'id1']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getId20()
	{
		return $this->hasOne(Item::className(), ['id' => 'id2']);
	}

	public function beforeValidate()
	{
		if ($this->id1 > $this->id2)
		{
			$tmp = $this->id1;
			$this->id1 = $this->id2;
			$this->id2 = $tmp;
		}
		return parent::beforeValidate();
	}
}
