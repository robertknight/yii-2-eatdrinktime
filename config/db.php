<?php

return [
	'class' => 'yii\db\Connection',
	// 'dsn' => 'pgsql:host=localhost;port=5432;dbname=eatdrinktime',
	'dsn' => 'mysql:host=localhost;dbname=eatdrinktime',
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
	'enableSchemaCache' => true,
];
