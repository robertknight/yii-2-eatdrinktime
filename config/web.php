<?php

$params = require(__DIR__ . '/params.php');

$config = [
	'id' => 'basic',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'components' => [
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'UsAp9zvJJRquMvU5OC2gwgZ94JbqxXpt',
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'user' => [
			// 'identityClass' => 'app\models\User',
			'identityClass' => 'app\persona\PersonaIdentity',
			'loginUrl' => ['site/index'],
			'returnUrl' => ['site/index'],
		],
		'assetManager' => [
			// 'linkAssets' => true,
			'bundles' => [
				'yii\web\JqueryAsset' => [
					'sourcePath' => null,
					'js' => ['//cdn.jsdelivr.net/jquery/2.1.1/jquery.min.js'],
				],
				'yii\bootstrap\BootstrapAsset' => [
					'sourcePath' => null,
					'css' => ['//cdn.jsdelivr.net/bootswatch/3.3.1.2/lumen/bootstrap.min.css'],
				],
				'yii\bootstrap\BootstrapPluginAsset' => [
					'sourcePath' => null,
					'js' => ['//cdn.jsdelivr.net/bootstrap/3.3.1/js/bootstrap.min.js'],
				],
				// 'yii\web\YiiAsset' => [
				// 	'sourcePath' => null,
				// 	'js' => ['js/yii.js'],
				// 	'depends' => ['yii\web\JqueryAsset']
				// ],
				// 'yii\validators\ValidationAsset' => [
				// 	'sourcePath' => null,
				// 	'js' => ['js/yii.validation.js'],
				// 	'depends' => ['yii\web\YiiAsset']
				// ],
				// 'yii\widgets\ActiveFormAsset' => [
				// 	'sourcePath' => null,
				// 	'js' => ['js/yii.activeForm.js'],
				// 	'depends' => ['yii\web\YiiAsset']
				// ],
				// 'yii\grid\GridViewAsset' => [
				// 	'sourcePath' => null,
				// 	'js' => ['js/yii.gridView.js'],
				// 	'depends' => ['yii\web\YiiAsset']
				// ],
			],
		],
		'urlManager' => [
			// 'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				'super-secret-login' => 'site/super-secret-login',
				'secure-signin' => 'site/secure-signin',
				'logout' => 'site/logout',
				'contact' => 'site/contact',

				'front' => 'site/index',
				'images' => 'image/index',
				'items' => 'item/index',
				'links' => 'item-item/index',
				'list' => 'item/list',

				'submit' => 'item/submit',

				'vote/<title:.+>/<id:\d+>' => 'item/vote',
				'submit-image/<title:.+>/<id:\d+>' => 'image/submit',
				'link-item/<title:.+>/<id:\d+>' => 'item-item/link',
				'update/item/<id:\d+>' => 'item/update',
				'images/item/<id:\d+>' => 'image/wall',
				'delete/item/<id:\d+>' => 'item/delete',

				'image/<id:\d+>' => 'image/view',
				'update/image/<id:\d+>' => 'image/update',
				'delete/image/<id:\d+>/direct/<direct:\d+>' => 'image/delete',
				'delete/image/<id:\d+>' => 'image/delete',

				'item-item/<id1:\d+>/<id2:\d+>' => 'item-item/view',
				'update/item-item/<id1:\d+>/<id2:\d+>' => 'item-item/update',
				'delete/item-item/<id1:\d+>/<id2:\d+>' => 'item-item/delete',

				'<title:.+>/<id:\d+>' => 'item/view',
				'<id:\d+>' => 'item/view'
			],
		],
		'view' => [
			'class' => 'yii\web\View',
			'renderers' => [
				'twig' => [
					'class' => 'yii\twig\ViewRenderer',
					'globals' => ['html' => '\yii\helpers\Html'],
					'uses' => ['yii\bootstrap'],
					'cachePath' => '@runtime/Twig/cache',
					'options' => [
						'auto_reload' => true
					]
				]
			]
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'db' => require(__DIR__ . '/db.php'),
	],
	'params' => $params,
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = 'yii\debug\Module';

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
