<?php

namespace app\persona;

use Yii;
use yii\web\IdentityInterface;

class PersonaIdentity implements IdentityInterface
{
	public $id;
	public $username;

	function __construct() {}

	public static function findIdentity($id)
	{
		$identity = new PersonaIdentity;
		$identity->id = 1;
		$identity->username = 'Robert';
		return $identity;
	}

	public static function findIdentityByAccessToken($token, $type = null) {}

	public function getId()
	{
		return $this->id;
	}

	public function getAuthKey() {}

	public function validateAuthKey($authKey) {}

	public function authenticate()
	{
		$persona = new Persona();

		if (!$persona->isVerified())
			return false;

		if (Yii::$app->params['email'] === $persona->getEmail())
		{
			$this->id = 1;
			$this->username = 'Robert';
			return true;
		}
		else
			return false;
	}
}
