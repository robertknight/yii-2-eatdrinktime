<?php

namespace app\persona;

use Yii;

class Persona
{
	private $_file;

	function __construct()
	{
		$assertion = Yii::$app->request->post('assertion');

		if ($assertion)
			$this->verifyAssertion($assertion);
	}

	private function verifyAssertion($assertion)
	{
		$options = [
			'http' => [
				'method' => 'POST',
				'header' => 'Content-Type: application/x-www-form-urlencoded',
				'content' => http_build_query(['assertion' => $assertion, 'audience' => 'https://eatdrinktime.com:443'])
			]
		];

		$context = stream_context_create($options);

		$this->_file = json_decode(file_get_contents('https://verifier.login.persona.org/verify', false, $context));
	}

	public function isVerified()
	{
		if ($this->_file)
			return ($this->_file->status === 'okay') ? true : false;
		else
			return false;
	}

	public function getEmail()
	{
		return $this->_file->email;
	}
}