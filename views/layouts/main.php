<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
// use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
	<div class="wrap">
		<header>
			<?php
				NavBar::begin([
					'brandLabel' => Yii::$app->params['app'],
					'brandUrl' => Yii::$app->homeUrl,
					'options' => [
						'class' => 'navbar navbar-inverse navbar-fixed-top',
						'role' => 'navigation'
					],
				]);
				echo Nav::widget([
					'options' => ['class' => 'navbar-nav navbar-left'],
					'items' => [
						['label' => 'Front', 'url' => ['/site/index']],
						['label' => 'List', 'url' => ['item/list']],
						['label' => 'Contact', 'url' => ['/site/contact']],
						Yii::$app->user->isGuest ?
							['label' => 'Login', 'url' => ['/site/login'], 'visible' => false] :
							['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
								'url' => ['/site/logout'],
								'linkOptions' => ['data-method' => 'post']],
					],
				]);
				NavBar::end(); ?>
		</header>

		<div class="container">
			<?= $content ?>
		</div>
	</div>

<?php $this->registerJS("$('#yiiFramework').tooltip();"); ?>
	<footer>
		<div class="container">
			<p class="pull-left">A side project by Robert Knight</p>
			<p class="pull-right" id='yiiFramework' data-toggle='tooltip' title='Yii 2 to be precise.'><?= Yii::powered() ?></p>
		</div>
	</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
