<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Image */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-form">

	<?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

	<?= $form->field($model, ($model->getScenario() === 'guestSubmit' || $model->getScenario() === 'update') ? 'file' : 'file[]')->widget(FileInput::classname(), [
		'options' => [
			'multiple' => ($model->getScenario() === 'guestSubmit' || $model->getScenario() === 'update') ? false : true,
			'accept' => 'image/*'
		],
		'pluginOptions' => [
			'previewFileType' => 'image',
			'browseClass' => 'btn btn-success',
			'uploadClass' => 'btn btn-primary',
			'removeClass' => 'btn btn-danger'
		]
	]); ?>

	<?php ActiveForm::end(); ?>

</div>
