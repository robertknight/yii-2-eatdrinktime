<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Image */
/* @var $title item title */

$this->title = 'Image '.$model['id'].' - '.Yii::$app->params['app'];
?>
<div class="image-view">

	<p>
		<?= Html::a('Images', ['wall', 'id' => $model['item_id']], ['class' => 'btn btn-black']) ?>
		<?= Html::a('Update', ['update', 'id' => $model['id']], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Delete', ['delete', 'id' => $model['id'], 'direct' => 1], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
		<?= Html::a('Index', ['index'], ['class' => 'btn btn-grey']) ?>
	</p>

	<div class='row'>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<a href='#' class="thumbnail">
				<img src="<?= Yii::$app->request->hostInfo.'/uploads/'.$model['file'] ?>" alt=''>
			</a>
		</div>
	</div>

</div>
