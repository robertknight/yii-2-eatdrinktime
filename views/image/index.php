<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Images - '.Yii::$app->params['app'];
?>
<div class="image-index">

	<p class='pull-right'>
		<?= Html::a('Items', ['item/index'], ['class' => 'btn btn-purple']) ?>
		<?= Html::a('Images', ['image/index'], ['class' => 'btn btn-black']) ?>
		<?= Html::a('Links', ['item-item/index'], ['class' => 'btn btn-warning']) ?>
	</p>

	<h1>Images</h1>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'item_id',
			'file',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
