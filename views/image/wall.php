<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $models app\models\Image */
/* @var $title item title */
/* @var $item_id item id */

$this->title = 'Images for '.$title.' - '.Yii::$app->params['app'];
?>
<div class="image-wall">

	<h1>Images</h1>

	<p><?= Html::a($title, ['item/view', 'id' => $item_id, 'title' => $title], ['class' => 'btn btn-default']) ?></p>

	<?php if ($models && count($models) > 0): ?>
		<div class="row">
		<?php foreach ($models as $model): ?>
			<div class="col-sm-2 col-xs-6">
				<a href="<?= Url::to(['view', 'id' => $model['id']]) ?>" class="thumbnail">
					<img src="<?= Yii::$app->request->hostInfo.'/uploads/'.$model['file'] ?>" alt=''>
				</a>
			</div>
		<?php endforeach ?>
		</div>
	<?php endif ?>

</div>
