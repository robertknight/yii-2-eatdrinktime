<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Image */

$this->title = 'Update Image - '.Yii::$app->params['app'];
?>
<div class="image-update">

	<h1>Update Image</h1>

	<p>
		<?= Html::a('Image', ['view', 'id' => $model['id']], ['class' => 'btn btn-black']) ?>
		<?= Html::a('Index', ['index'], ['class' => 'btn btn-grey']) ?>
	</p>

	<?= $this->render('_form', ['model' => $model]) ?>

	<div class='row'>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<a href='#' class="thumbnail">
				<img src="<?= Yii::$app->request->hostInfo.'/uploads/'.$model['file'] ?>" alt=''>
			</a>
		</div>
	</div>

</div>
