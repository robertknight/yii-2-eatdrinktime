<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Image */
/* @var $id item id */
/* @var $title item title */

$this->title = 'Submit Image to '.$title.' - '.Yii::$app->params['app'];
?>
<div class="image-submit">

	<h1>Submit Image</h1>

	<p><?= Html::a($title, ['item/view', 'id' => $id, 'title' => $title], ['class' => 'btn btn-default']) ?></p>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
