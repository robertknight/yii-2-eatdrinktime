<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Links - '.Yii::$app->params['app'];
?>
<div class="item-item-index">

	<p class='pull-right'>
		<?= Html::a('Items', ['item/index'], ['class' => 'btn btn-purple']) ?>
		<?= Html::a('Images', ['image/index'], ['class' => 'btn btn-black']) ?>
		<?= Html::a('Links', ['item-item/index'], ['class' => 'btn btn-warning']) ?>
	</p>

	<h1>Links</h1>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id1',
			'id2',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
