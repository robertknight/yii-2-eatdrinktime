<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\ItemItem */
/* @var $form yii\widgets\ActiveForm */
/* @var $id item id */
/* @var $title item title */
/* @var $linkableItems item links */

$this->title = 'Link Item to '.$title.' - '.Yii::$app->params['app'];
?>
<div class="item-item-link">

	<h1>Link Item</h1>
	<h5>Linking an item will "link" or associate this item to the linking item.</h5>

	<p><?= Html::a($title, ['item/view', 'id' => $id, 'title' => $title], ['class' => 'btn btn-default']) ?></p>

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'links')->widget(Select2::classname(), [
		'data' => $linkableItems,
		'size' => 'sm',
		'options' => ['multiple' => true],
		'pluginOptions' => [
			'maximumInputLength' => 70
		],
	]); ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Link' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-purple' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
