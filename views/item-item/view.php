<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ItemItem */
/* @var $titles item titles */

$this->title = 'Item Link - '.Yii::$app->params['app'];
?>
<div class="item-item-view">

	<h1>Item Link</h1>

	<p>
		<?= Html::a($titles['0'], ['item/view', 'id' => $model->id1, 'title' => $titles['0']], ['class' => 'btn btn-default']) ?>
		<?= Html::a($titles['1'], ['item/view', 'id' => $model->id2, 'title' => $titles['1']], ['class' => 'btn btn-default']) ?>
		<?= Html::a('Delete', ['delete', 'id1' => $model->id1, 'id2' => $model->id2], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
		<?= Html::a('Index', ['index'], ['class' => 'btn btn-light-orange']) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id1',
			'id2',
		],
	]) ?>

</div>
