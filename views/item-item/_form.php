<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ItemItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-item-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'id1')->textInput() ?>

	<?= $form->field($model, 'id2')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Link' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
