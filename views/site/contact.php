<?php

/* @var $this yii\web\View */

$this->title = 'Contact - '.Yii::$app->params['app'];
?>
<div class="site-contact">
	<h1>Contact</h1>

	<form method="POST" action="https://formspree.io/robrtknight@gmail.com" role="form" autocomplete="off">
		<div class="form-group">
			<label for="email">Email</label>
			<input type="email" name="_replyto" class="form-control input-sm" id="email" placeholder="Enter email" required>
		</div>
		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" name="name" class="form-control input-sm" id="name" placeholder="Enter name" required>
		</div>
		<div class="form-group">
			<label for="subject">Subject</label>
			<input type="text" name="subject" class="form-control input-sm" id="subject" placeholder="Enter subject" required>
		</div>
		<div class="form-group">
			<label for="message">Message</label>
			<textarea name="message" class="form-control input-sm" id="message" rows="6" placeholder="Enter message" required></textarea>
		</div>
		<input type="hidden" name="_subject" value="Contact - EatDrinkTime">
		<button type="submit" class="btn btn-primary btn-sm">Submit</button>
	</form>

</div>
