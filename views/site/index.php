<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $chartItems */
/* @var $popularFoodItems */
/* @var $popularDrinkItems */
/* @var $recentItems */

$this->title = 'EatDrinkTime by Robert Knight';
?>

<?php $this->registerMetaTag(['name' => 'description', 'content' => 'A database-driven web site about consumption times.']) ?>
<?php $this->registerCssFile('//cdn.jsdelivr.net/morris.js/0.5.1/morris.css') ?>
<?php $this->registerJsFile('//cdn.jsdelivr.net/raphael/2.1.2/raphael-min.js', ['depends' => ['yii\web\JqueryAsset']]) ?>
<?php $this->registerJsFile('//cdn.jsdelivr.net/morris.js/0.5.1/morris.min.js', ['depends' => ['yii\web\JqueryAsset']]) ?>
<?php foreach ($chartItems as $i => $chartItem): ?>
	<?php $this->registerJS("
	new Morris.Donut({
		element: 'chart".$i."',
		data: [
			{ label: 'anytime', value: ".Html::encode($chartItem['anytime'])." },
			{ label: 'night', value: ".Html::encode($chartItem['night'])." },
			{ label: 'afternoon', value: ".Html::encode($chartItem['afternoon'])." },
			{ label: 'morning', value: ".Html::encode($chartItem['morning'])." }
		],
		resize: true,
		colors: ['#0A558F','#118EEE','#70BBF5','#CFE8FC']
	});
	");
	?>
<?php endforeach ?>

<div class="site-index">

<?php if (!Yii::$app->user->isGuest): ?>
	<p class='pull-right'>
		<?= Html::a('Items', ['item/index'], ['class' => 'btn btn-purple']) ?>
		<?= Html::a('Images', ['image/index'], ['class' => 'btn btn-black']) ?>
		<?= Html::a('Links', ['item-item/index'], ['class' => 'btn btn-warning']) ?>
	</p>
<?php endif ?>

	<section class="jumbotron">
		<h1>A database of consumption times.</h1>
		<p class="lead">Record what time you like to have your coffee or when is a good time for cake? Get insight to eat/drink patterns.</p>
		<?= Html::a('Submit item', ['item/submit'], ['class' => 'btn btn-success btn-lg']) ?>
	</section>

	<section>
		<h1>What time do people have:</h1>
		<div class='row'>
<?php foreach ($chartItems as $i => $chartItem): ?>
			<a href="<?= Html::encode(Url::to(['item/view', 'id' => $chartItem['id'], 'title' => $chartItem['title']])) ?>">
				<div class='pane col-md-2 col-sm-4 col-xs-12'>
					<h2><?= Html::encode($chartItem['title']) ?></h2>
					<div id="chart<?= $i ?>"></div>
				</div>
			</a>
<?php endforeach ?>
		</div>
	</section>

	<section>
		<h1>Most popular food items:</h1>
<?php if ($popularFoodItems): ?>
		<div class="row">
<?php foreach ($popularFoodItems as $id => $item): ?>
<?php $this->registerJS("$('#itemPF".Html::encode($id)."').tooltip();"); ?>
<?php if ($item->file): ?>
			<div class="col-sm-2 col-xs-6" id="itemPF<?= Html::encode($id) ?>" data-toggle='tooltip' title="<?= Html::encode($item->title) ?>">
				<a href="<?= Html::encode(Url::to(['item/view', 'id' => $id, 'title' => $item->title])) ?>" class="thumbnail">
					<img src="uploads/thumbnails/150x150_<?= Html::encode($item->file) ?>" alt='<?= Html::encode($item->title) ?>'>
				</a>
			</div>
<?php else: ?>
			<div class="col-sm-2 col-xs-6" id="itemPF<?= Html::encode($id) ?>" data-toggle='tooltip' title="<?= Html::encode($item->title) ?>">
				<a href="<?= Html::encode(Url::to(['item/view', 'id' => $id, 'title' => $item->title])) ?>" class="thumbnail">
					<img src='placeholder.png' alt='No Image'>
				</a>
			</div>
<?php endif ?>
<?php endforeach ?>
		</div>
<?php endif ?>
	</section>

	<section>
	<h1>Most popular drink items:</h1>
<?php if ($popularDrinkItems): ?>
	<div class="row">
<?php foreach ($popularDrinkItems as $id => $item): ?>
<?php $this->registerJS("$('#itemPD".Html::encode($id)."').tooltip();"); ?>
<?php if ($item->file): ?>
		<div class="col-sm-2 col-xs-6" id="itemPD<?= Html::encode($id) ?>" data-toggle='tooltip' title="<?= Html::encode($item->title) ?>">
			<a href="<?= Html::encode(Url::to(['item/view', 'id' => $id, 'title' => $item->title])) ?>" class="thumbnail">
				<img src="uploads/thumbnails/150x150_<?= Html::encode($item->file) ?>" alt='<?= Html::encode($item->title) ?>'>
			</a>
		</div>
<?php else: ?>
		<div class="col-sm-2 col-xs-6" id="itemPD<?= Html::encode($id) ?>" data-toggle='tooltip' title="<?= Html::encode($item->title) ?>">
			<a href="<?= Html::encode(Url::to(['item/view', 'id' => $id, 'title' => $item->title])) ?>" class="thumbnail">
				<img src='placeholder.png' alt='No Image'>
			</a>
		</div>
<?php endif ?>
<?php endforeach ?>
	</div>
<?php endif ?>
	</section>

	<section>
		<h1>Most recent items:</h1>
<?php if ($recentItems): ?>
		<div class="row">
<?php foreach ($recentItems as $id => $item): ?>
<?php $this->registerJS("$('#itemR".Html::encode($id)."').tooltip();"); ?>
<?php if ($item->file): ?>
			<div class="col-md-1 col-sm-2 col-xs-6" id="itemR<?= Html::encode($id) ?>" data-toggle='tooltip' title="<?= Html::encode($item->title) ?>">
				<a href="<?= Html::encode(Url::to(['item/view', 'id' => $id, 'title' => $item->title])) ?>" class="thumbnail">
					<img src="uploads/thumbnails/150x150_<?= Html::encode($item->file) ?>" alt='<?= Html::encode($item->title) ?>'>
				</a>
			</div>
<?php else: ?>
			<div class="col-md-1 col-sm-2 col-xs-6" id="itemR<?= Html::encode($id) ?>" data-toggle='tooltip' title="<?= Html::encode($item->title) ?>">
				<a href="<?= Html::encode(Url::to(['item/view', 'id' => $id, 'title' => $item->title])) ?>" class="thumbnail">
					<img src='placeholder.png' alt='No Image'>
				</a>
			</div>
<?php endif ?>
<?php endforeach ?>
		</div>
<?php endif ?>
	</section>

</div>
