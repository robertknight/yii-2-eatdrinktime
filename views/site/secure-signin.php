<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Secure Signin - '.Yii::$app->params['app'];
?>
<?php $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex']) ?>
<?php $this->registerMetaTag(['http-equiv' => 'X-UA-Compatible', 'content' => 'IE=Edge'], 'IE-Edge'); ?>
<?php $this->registerJsFile('//login.persona.org/include.js'); ?>
<?php	$this->registerJS('
$("#persona-btn").click(function(event) {
	event.preventDefault();
	navigator.id.request();
});

navigator.id.watch({
	loggedInUser: null,
	onlogin: function(assertion) {
		$.post(
			"'.Html::encode(Url::to(['secure-signin'])).'",
			{ assertion: assertion }
		);
	},
	onlogout: function() {}
});
', $this::POS_END, 'persona');
?>

<div class="site-secure-signin">
	<h1>Secure Signin</h1>
	<a id='persona-btn' href='#'><img src='persona.gif'></a>
</div>
