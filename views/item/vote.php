<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vote */
/* @var $id item id */
/* @var $title item title */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Vote on '.$title.' - '.Yii::$app->params['app'];
?>
<div class="item-vote">

	<h1>Vote</h1>
	<h5>What time do you normally consume this item?<br>Each check mark counts as a vote.<br>When all three boxes are checked, a single vote will be placed for anytime.</h5>

	<p><?= Html::a($title, ['view', 'id' => $id, 'title' => $title], ['class' => 'btn btn-default']) ?></p>

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'times')->checkboxList(['morning' => 'morning', 'afternoon' => 'afternoon', 'night' => 'night'],
	 ['separator' => '<br>']); ?>

	<div class="form-group">
		<?= Html::submitButton('Vote', ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
