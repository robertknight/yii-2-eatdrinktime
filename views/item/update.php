<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Item */

$this->title = 'Update Item - '.Yii::$app->params['app'];
?>
<div class="item-update">

	<h1>Update Item</h1>

	<p>
		<?= Html::a($model->title, ['view', 'id' => $model->id, 'title' => $model->title], ['class' => 'btn btn-default']) ?>
		<?= Html::a('Index', ['index'], ['class' => 'btn btn-light-purple']) ?>
	</p>

	<?= $this->render('_form', ['model' => $model]) ?>

</div>
