<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\SimpleItem */
/* @var $items all the items in the db */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'List - '.Yii::$app->params['app'];
?>
<?php $this->registerJS("$('#inspect').tooltip();"); ?>

<div class="item-list">

	<p class='pull-right'>
		<?= Html::a('Submit Item', ['submit'], ['class' => 'btn btn-success']) ?>
	</p>

	<h1>List</h1>
	<h5>The List shows all the items that exist in the database.</h5>

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'id')->widget(Select2::classname(), [
		'data' => $items,
		'size' => 'sm',
		'options' => ['placeholder' => 'Search'],
		'pluginOptions' => [
			'allowClear' => true,
			'maximumInputLength' => 70
		],
		'addon' => ['append' => [
			'content' => Html::submitButton("<span class='glyphicon glyphicon-search'></span>", [
				'class' => 'btn btn-primary',
				'id' => 'inspect',
				'data-toggle' => 'tooltip',
				'title' => 'Inspect'
			]),
			'asButton' => true
		]]
	]); ?>

	<?php ActiveForm::end(); ?>

	<?= ListView::widget([
		'dataProvider' => $dataProvider,
		'itemOptions' => ['class' => 'item'],
		'itemView' => function ($model, $key, $index, $widget) {
			return Html::a(Html::encode($model['title']), ['view', 'id' => $model['id'], 'title' => $model['title']]);
		}
	]) ?>

</div>
