<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Items - '.Yii::$app->params['app'];
?>
<div class="item-index">

	<p class='pull-right'>
		<?= Html::a('Submit Item', ['submit'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('Items', ['item/index'], ['class' => 'btn btn-purple']) ?>
		<?= Html::a('Images', ['image/index'], ['class' => 'btn btn-black']) ?>
		<?= Html::a('Links', ['item-item/index'], ['class' => 'btn btn-warning']) ?>
	</p>

	<h1>Items</h1>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'title',
			'type',
			'morning',
			'afternoon',
			'night',
			'anytime',
			'timestamp',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
