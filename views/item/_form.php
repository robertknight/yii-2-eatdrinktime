<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">

	<?php $form = ActiveForm::begin(['options' => ['autocomplete'=>'off']]); ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => 70]) ?>

	<?= $form->field($model, 'type')->widget(SwitchInput::classname(), [
		'pluginOptions' => [
			'size' => 'small',
			'onText' => 'drink',
			'offText' => 'food'
		],
		'inlineLabel' => false,
	]); ?>

	<?php if ($model->isNewRecord): ?>
	<?= $form->field($model, 'times')->checkboxList(['morning' => 'morning', 'afternoon' => 'afternoon', 'night' => 'night'],
	 ['separator' => '<br>']); ?>
	<?php endif ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-info']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
