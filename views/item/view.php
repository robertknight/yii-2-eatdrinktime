<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Item */
/* @var $votes votes */
/* @var $percents percents */
/* @var $images images */
/* @var $links links */

$this->title = $model['title'].' - '.Yii::$app->params['app'];
?>
<?php if ($images): ?>
<?php $this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.2/fotorama.css') ?>
<?php $this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.2/fotorama.js', ['depends' => ['yii\web\JqueryAsset']]) ?>
<?php endif ?>

<div class="item-view">
	<article>
		<p id='controls'>
			<?= Html::a('Vote', ['vote', 'id' => $model['id'], 'title' => $model['title']], ['class' => 'btn btn-primary']) ?>
			<?= Html::a('Submit image', ['image/submit', 'id' => $model['id'], 'title' => $model['title']], ['class' => 'btn btn-success']) ?>
			<?= Html::a('Link item', ['item-item/link', 'id' => $model['id'], 'title' => $model['title']], ['class' => 'btn btn-purple']) ?>
		<?php if (!Yii::$app->user->isGuest): ?>
			<?= Html::a('Update', ['update', 'id' => $model['id']], ['class' => 'btn btn-info']) ?>
			<?= Html::a('Images', ['image/wall', 'id' => $model['id']], ['class' => 'btn btn-black']) ?>
			<?= Html::a('Delete', ['delete', 'id' => $model['id']], [
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => 'Are you sure you want to delete this item?',
					'method' => 'post',
				],
			]) ?>
			<?= Html::a('Index', ['index'], ['class' => 'btn btn-light-purple']) ?>
		<?php endif ?>
		</p>

		<h1><?= Html::encode($model['title']) ?></h1>

		<h2><?= Html::encode(number_format($votes)).' '.(($votes===1)?'vote':'votes') ?></h2>
		<h4>
			<span><?= Html::encode($percents['morning']) ?>% morning |</span>
			<span><?= Html::encode($percents['afternoon']) ?>% afternoon |</span>
			<span><?= Html::encode($percents['night']) ?>% night |</span>
			<span><?= Html::encode($percents['anytime']) ?>% anytime</span>
		</h4>

		<div align='center' class='fotorama'
			data-keyboard='{"space": true, "home": true, "end": true, "left": true, "right": true}'
			data-navposition='bottom' data-loop='true' data-nav='thumbs'
			data-maxheight='400' data-width='100%' data-ratio='16/9'>
			<?php
			if ($images)
				foreach ($images as $image)
					if ($image): ?>
						<a href="<?= Yii::$app->request->hostInfo.'/uploads/'.Html::encode($image['file']) ?>">
							<img src="<?= Yii::$app->request->hostInfo.'/uploads/thumbnails/64x64_'.Html::encode($image['file']) ?>" alt='<?= Html::encode($model['title']) ?>'>
						</a>
					<?php endif	?>
		</div>

	<?php if ($links): ?>
		<hr>

		<h4>Linked Items</h4>

		<div class="row">
	<?php foreach ($links as $id => $link): ?>
	<?php $this->registerJS("$('#link".Html::encode($id)."').tooltip();"); ?>
			<div class="col-md-1 col-sm-2 col-xs-6" id="link<?= Html::encode($id) ?>" data-toggle='tooltip' title="<?= Html::encode($link->title) ?>">
				<a href="<?= Html::encode(Url::to(['view', 'id' => $id, 'title' => $link->title])) ?>" class="thumbnail">
	<?php if ($link->file): ?>
					<img src="<?= Yii::$app->request->hostInfo.'/uploads/thumbnails/150x150_'.Html::encode($link->file) ?>" alt='<?= Html::encode($link->title) ?>'>
	<?php else: ?>
					<img src="<?= Yii::$app->request->hostInfo.'/placeholder.png' ?>" alt='No Image'>
	<?php endif ?>
				</a>
			</div>
	<?php endforeach ?>
		</div>
	<?php endif ?>
	</article>

	<div id="disqus_thread"></div>
	<script type="text/javascript">
		/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
		var disqus_shortname = 'eatdrinktime'; // required: replace example with your forum shortname
		var disqus_identifier = '<?= Html::encode($model['id']) ?>';
		var disqus_title = '<?= Html::encode($model['title']) ?>';
		var disqus_url = "<?= Html::encode(Url::to(['view', 'id' => $model['id'], 'title' => $model['title']], true)) ?>";

		/* * * DON'T EDIT BELOW THIS LINE * * */
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
	</script>
	<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>

</div>
