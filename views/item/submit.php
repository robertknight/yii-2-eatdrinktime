<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Item */

$this->title = 'Submit Item - '.Yii::$app->params['app'];
?>
<div class="item-submit">

	<h1>Submit Item</h1>
	<h5>Before submitting, please make sure the item doesn't already exist in the database by checking out the
	<a href='<?= Url::to(['item/list']) ?>'>List</a>.</h5>

	<?= $this->render('_form', ['model' => $model]) ?>

</div>
