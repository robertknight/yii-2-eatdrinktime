<?php

namespace app\controllers;

use Yii;
use app\models\Item;
use app\models\ItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use app\models\Image;
use app\models\ItemItem;
use app\models\ListItem;
use app\models\Vote;
use yii\data\ArrayDataProvider;
use app\models\SimpleItem;
use yii\filters\AccessControl;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','list','view','submit','vote','update','delete'],
				'rules' => [
					[
						'allow' => true,
						'actions' => ['list','view','submit','vote']
					],
					[
						'allow' => true,
						'actions' => ['index','update','delete'],
						'roles' => ['@'],
					],
				],
				'denyCallback' => function ($rule, $action) {
					throw new NotFoundHttpException('The requested page does not exist.');
				}
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all Item models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ItemSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionList()
	{
		$model = new ListItem;

		$items = [];
		$dataProvider;

		if ($model->load(Yii::$app->request->post()))
			if ($model->id)
				return $this->redirect(['view', 'id' => $model->id]);
			else
				$this->listHelper($items, $dataProvider);
		else
			$this->listHelper($items, $dataProvider);

		return $this->render('list', ['model' => $model, 'items' => $items, 'dataProvider' => $dataProvider]);
	}

	private function listHelper(&$items, &$dataProvider)
	{
		$itemRows = Yii::$app->db->createCommand('
			SELECT id, title
			FROM item
			ORDER BY title
		')->queryAll();

		foreach ($itemRows as $row)
			$items[$row['id']] = $row['title'];

		$dataProvider = new ArrayDataProvider([
			'allModels' => $itemRows,
			'pagination' => ['pageSize' => 20]
		]);
	}

	/**
	 * Displays a single Item model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$itemSQL = Yii::$app->db->createCommand('
			SELECT id, title, morning, afternoon, night, anytime
			FROM item
			WHERE id = :id
		');
		$itemSQL->bindValue(':id', $id);
		$itemRow = $itemSQL->queryOne();
		if (!$itemRow)
			throw new NotFoundHttpException('The requested page does not exist.');

		$votes = $itemRow['morning'] + $itemRow['afternoon'] + $itemRow['night'] + $itemRow['anytime'];

		if ($votes === 0)
			$percents = ['morning' => 0, 'afternoon' => 0, 'night' => 0, 'anytime' => 0];
		else
		{
			$percents = [
				'morning' => $itemRow['morning'] / $votes * 100,
				'afternoon' => $itemRow['afternoon'] / $votes * 100,
				'night' => $itemRow['night'] / $votes * 100,
				'anytime' => $itemRow['anytime'] / $votes * 100
			];

			foreach ($percents as $i => $percent)
				if (is_float($percent) && (floor($percent) !== $percent))
					$percents[$i] = number_format($percent, 1);
		}

		// PostgreSQL
		// $imageSQL = Yii::$app->db->createCommand('
		// 	SELECT file
		// 	FROM image
		// 	WHERE item_id = :item_id
		// 	ORDER BY RANDOM()
		// ');

		// MariaDB
		$imageSQL = Yii::$app->db->createCommand('
			SELECT file
			FROM image
			WHERE item_id = :item_id
			ORDER BY RAND()
		');
		$imageSQL->bindValue(':item_id', $id);
		$images = $imageSQL->queryAll();

		return $this->render('view', [
			'model' => $itemRow,
			'votes' => $votes,
			'percents' => $percents,
			'images' => $images,
			'links' => $this->getLinkItems($id)
		]);
	}

	private function getLinkItems($id)
	{
		$links = [];

		$itemSQL = Yii::$app->db->createCommand('
			SELECT id, title
			FROM item
			WHERE id <> :id AND (
				id IN (SELECT id1 FROM item_item WHERE id2 = :id2)
				OR
				id IN (SELECT id2 FROM item_item WHERE id1 = :id1)
			)
			ORDER BY title
		');
		$itemSQL->bindValue(':id', $id);
		$itemSQL->bindValue(':id1', $id);
		$itemSQL->bindValue(':id2', $id);
		$itemLinkRows = $itemSQL->queryAll();

		foreach ($itemLinkRows as $row)
		{
			// PostgreSQL
			// $imageSQL = Yii::$app->db->createCommand('
			// 	SELECT file
			// 	FROM image
			// 	WHERE item_id = :id
			// 	ORDER BY RANDOM()
			// 	LIMIT 5
			// ');

			// MariaDB
			$imageSQL = Yii::$app->db->createCommand('
				SELECT file
				FROM image
				WHERE item_id = :id
				ORDER BY RAND()
				LIMIT 5
			');
			$imageSQL->bindValue(':id', $row['id']);
			$imageRows = $imageSQL->queryAll();

			$links[$row['id']] = new SimpleItem;
			$links[$row['id']]->title = $row['title'];

			if ($imageRows)
				$links[$row['id']]->file = $imageRows[0]['file'];
			else
				$links[$row['id']]->file = null;
		}

		return $links;
	}

	/**
	 * Creates a new Item model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionSubmit()
	{
		$model = new Item();

		if ($model->load(Yii::$app->request->post()))
		{
			if ($model->times)
			{
				if (count($model->times) === 3)
					$model->anytime += 1;
				else
					foreach ($model->times as $time)
						switch ($time)
						{
							case 'morning':
								$model->morning += 1;
								break;
							case 'afternoon':
								$model->afternoon += 1;
								break;
							case 'night':
								$model->night += 1;
								break;
						}
			}

			$model->timestamp = date('Y-m-d H:i:s');

			if ($model->save())
				return $this->redirect(['view', 'id' => $model->id, 'title' => $model->title]);
			else
				return $this->render('submit', ['model' => $model]);
		}
		else
			return $this->render('submit', ['model' => $model]);
	}

	public function actionVote($id, $title)
	{
		$model = new Vote;

		if ($model->load(Yii::$app->request->post()))
		{
			if ($model->times)
			{
				$item = Item::find()
					->select(['id','morning','afternoon','night','anytime'])
					->where('id=:id', [':id' => $id])
					->one();
				if (!$item)
					throw new NotFoundHttpException('The requested page does not exist.');

				if (count($model->times) === 3)
					$item->anytime += 1;
				else
					foreach ($model->times as $time)
						switch ($time)
						{
							case 'morning':
								$item->morning += 1;
								break;
							case 'afternoon':
								$item->afternoon += 1;
								break;
							case 'night':
								$item->night += 1;
								break;
						}

				if ($item->update(false))
					return $this->redirect(['view', 'id' => $id, 'title' => $title]);
				else
					return $this->render('vote.twig', [
						'model' => $model,
						'id' => $id,
						'title' => $title
					]);
			}
			else
				return $this->redirect(['view', 'id' => $id, 'title' => $title]);
		}
		else
			return $this->render('vote.twig', [
				'model' => $model,
				'id' => $id,
				'title' => $title
			]);
	}

	/**
	 * Updates an existing Item model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = Item::find()
			->select(['id','title','type'])
			->where('id=:id', [':id' => $id])
			->one();
		if (!$model)
			throw new NotFoundHttpException('The requested page does not exist.');

		$model->setScenario('update');

		if ($model->load(Yii::$app->request->post()) && $model->save())
			return $this->redirect(['view', 'id' => $model->id, 'title' => $model->title]);
		else
			return $this->render('update', ['model' => $model]);
	}

	/**
	 * Deletes an existing Item model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = Item::find()
			->select('id')
			->where('id=:id', [':id' => $id])
			->one();
		if (!$model)
			throw new NotFoundHttpException('The requested page does not exist.');

		$imageRows = (new Query)
			->select('file')
			->from(Image::tableName())
			->where('item_id=:item_id', [':item_id' => $id])
			->all();

		$images = [];

		foreach ($imageRows as $imageRow)
			if ($imageRow['file'])
				$images[] = $imageRow['file'];

		if ($model->delete())
			foreach ($images as $image)
			{
				unlink('uploads/'.$image);
				unlink('uploads/thumbnails/150x150_'.$image);
				unlink('uploads/thumbnails/64x64_'.$image);
			}

		return $this->redirect(['index']);
	}
}
