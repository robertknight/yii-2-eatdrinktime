<?php

namespace app\controllers;

use Yii;
use app\models\Image;
use app\models\ImageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\imagine\Image as Imagine;

/**
 * ImageController implements the CRUD actions for Image model.
 */
class ImageController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','view','wall','submit','update','delete'],
				'rules' => [
					[
						'allow' => true,
						'actions' => ['submit']
					],
					[
						'allow' => true,
						'actions' => ['index','view','wall','update','delete'],
						'roles' => ['@'],
					],
				],
				'denyCallback' => function ($rule, $action) {
					throw new NotFoundHttpException('The requested page does not exist.');
				}
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all Image models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ImageSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Image model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$imageRow = (new Query)
			->select(['id','item_id','file'])
			->from(Image::tableName())
			->where('id=:id', [':id' => $id])
			->one();
		if (!$imageRow)
			throw new NotFoundHttpException('The requested page does not exist.');

		$itemRow = (new Query)
			->select('title')
			->from('item')
			->where('id=:id', [':id' => $imageRow['item_id']])
			->one();
		if (!$itemRow)
			throw new NotFoundHttpException('The requested page does not exist.');

		return $this->render('view', [
			'model' => $imageRow,
			'title' => $itemRow['title']
		]);
	}

	public function actionWall($id)
	{
		$imageRows = (new Query)
			->select(['id','file'])
			->from(Image::tableName())
			->where('item_id=:item_id', [':item_id' => $id])
			->all();

		$itemRow = (new Query)
			->select('title')
			->from('item')
			->where('id=:id', [':id' => $id])
			->one();

		return $this->render('wall', [
			'models' => $imageRows,
			'title' => $itemRow['title'],
			'item_id' => $id
		]);
	}

	/**
	 * Creates a new Image model.
	 * If creation is successful, the browser will be redirected to the 'item/view' page.
	 * @return mixed
	 */
	public function actionSubmit($id, $title)
	{
		$model = new Image;

		if (Yii::$app->user->isGuest)
			$model->setScenario('guestSubmit');
		else
			$model->setScenario('adminSubmit');

		if (Yii::$app->request->post())
		{
			$models = [];
			$files = UploadedFile::getInstances($model, 'file');

			foreach ($files as $file)
			{
				$model = new Image;
				$model->item_id = $id;
				$model->file = $file;
				$models[] = $model;
			}

			// All or nothing image saving.
			foreach ($models as $model)
				if (!$model->validate() || !$model->file || $model->file->hasError)
					return $this->render('submit', [
						'model' => $model,
						'id' => $id,
						'title' => $title
					]);

			// All images are valid and have passed all tests.
			foreach ($models as $i => $model)
			{
				$model->file = $id.date('_Ymd_His_').substr((string)microtime(), 2, 6).'.'.$files[$i]->extension;
				if ($model->save(false))
				{
					$files[$i]->saveAs('uploads/'.$model->file);

					Imagine::thumbnail('uploads/'.$model->file, 150, 150)
						->save('uploads/thumbnails/150x150_'.$model->file, ['quality' => 50]);

					Imagine::thumbnail('uploads/'.$model->file, 64, 64)
						->save('uploads/thumbnails/64x64_'.$model->file, ['quality' => 50]);
				}
			}

			return $this->redirect(['item/view', 'id' => $id, 'title' => $title]);
		}
		else
			return $this->render('submit', [
				'model' => $model,
				'id' => $id,
				'title' => $title
			]);
	}

	/**
	 * Updates an existing Image model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = Image::find()
			->select(['id','file'])
			->where('id=:id', [':id' => $id])
			->one();
		if (!$model)
			throw new NotFoundHttpException('The requested page does not exist.');

		$model->setScenario('update');

		if (Yii::$app->request->post())
		{
			$current_file = $model->file;
			$file_name = substr($model->file, 0, strpos($model->file, '.'));

			$file = UploadedFile::getInstance($model,'file');

			if ($file && !$file->hasError)
				$model->file = $file;

			if (!$model->validate())
				return $this->render('update', ['model' => $model]);

			$model->file = $file_name.'.'.$file->extension;

			if ($model->save(false))
			{
				unlink('uploads/'.$current_file);
				unlink('uploads/thumbnails/150x150_'.$current_file);
				unlink('uploads/thumbnails/64x64_'.$current_file);

				$file->saveAs('uploads/'.$model->file);

				Imagine::thumbnail('uploads/'.$model->file, 150, 150)
					->save('uploads/thumbnails/150x150_'.$model->file, ['quality' => 50]);

				Imagine::thumbnail('uploads/'.$model->file, 64, 64)
					->save('uploads/thumbnails/64x64_'.$model->file, ['quality' => 50]);
			}

			return $this->redirect(['view', 'id' => $model->id]);
		}
		else
			return $this->render('update', ['model' => $model]);
	}

	/**
	 * Deletes an existing Image model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id, $direct = null)
	{
		$model = Image::find()
			->select(['id','item_id','file'])
			->where('id=:id', [':id' => $id])
			->with([
				'item' => function($query) {
					$query->select('title');
				}
			])
			->one();
		if (!$model)
			throw new NotFoundHttpException('The requested page does not exist.');

		$item_id = $model->item_id;
		$file = $model->file;
		$title = $model->item->title;

		if ($model->delete())
		{
			unlink('uploads/'.$file);
			unlink('uploads/thumbnails/150x150_'.$file);
			unlink('uploads/thumbnails/64x64_'.$file);
		}

		if ($direct)
			return $this->redirect(['wall', 'id' => $item_id, 'title' => $title]);
		else
			return $this->redirect(['index']);
	}
}
