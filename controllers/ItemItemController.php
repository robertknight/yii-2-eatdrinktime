<?php

namespace app\controllers;

use Yii;
use app\models\ItemItem;
use app\models\ItemItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\filters\AccessControl;

/**
 * ItemItemController implements the CRUD actions for ItemItem model.
 */
class ItemItemController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','view','link','update','delete'],
				'rules' => [
					[
						'allow' => true,
						'actions' => ['link']
					],
					[
						'allow' => true,
						'actions' => ['index','view','update','delete'],
						'roles' => ['@'],
					],
				],
				'denyCallback' => function ($rule, $action) {
					throw new NotFoundHttpException('The requested page does not exist.');
				}
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all ItemItem models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ItemItemSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single ItemItem model.
	 * @param integer $id1
	 * @param integer $id2
	 * @return mixed
	 */
	public function actionView($id1, $id2)
	{
		$titles = [];

		// Query Builder – Quick performance and concise presentation in this case. Seven lines of code.
		$row = (new Query)
			->select('title')
			->from('item')
			->where('id=:id', [':id' => $id1])
			->one();
		if (!$row)
			throw new NotFoundHttpException('The requested page does not exist.');

		$titles[] = $row['title'];

		// Raw SQL – The fastest, but takes more work. Nine lines of code. A little harder to read.
		$itemSQL = Yii::$app->db->createCommand('
			SELECT title
			FROM item
			WHERE id = :id
		');
		$itemSQL->bindValue(':id', $id2);

		$row = $itemSQL->queryOne();
		if (!$row)
			throw new NotFoundHttpException('The requested page does not exist.');

		$titles[] = $row['title'];

		return $this->render('view', ['model' => $this->findModel($id1, $id2), 'titles' => $titles]);
	}

	public function actionLink($id, $title)
	{
		$model = new ItemItem();

		$linkableItems = $this->getLinkableItems($id);

		if ($model->load(Yii::$app->request->post()))
		{
			if ($model->links)
			{
				$models = [];

				foreach ($model->links as $link)
				{
					$model = new ItemItem();
					$model->id1 = $id;
					$model->id2 = $link;
					$models[] = $model;
				}

				// All or nothing link checking.
				foreach ($models as $model)
					if (!$model->validate())
						return $this->render('link', [
							'model' => $model,
							'id' => $id,
							'title' => $title,
							'linkableItems' => $linkableItems
						]);

				// All links are valid and have passed all tests.
				foreach ($models as $model)
					$model->save(false);

				return $this->redirect(['item/view', 'id' => $id, 'title' => $title]);
			}
			else
				return $this->redirect(['item/view', 'id' => $id, 'title' => $title]);
		}
		else
			return $this->render('link', [
				'model' => $model,
				'id' => $id,
				'title' => $title,
				'linkableItems' => $linkableItems
			]);
	}

	private function getLinkableItems($id)
	{
		$items = [];

		$itemSQL = Yii::$app->db->createCommand('
			SELECT id, title
			FROM item
			WHERE id <> :id
				AND	id NOT IN (SELECT id1 FROM item_item WHERE id2 = :id2)
				AND	id NOT IN (SELECT id2 FROM item_item WHERE id1 = :id1)
			ORDER BY title
		');
		$itemSQL->bindValue(':id', $id);
		$itemSQL->bindValue(':id1', $id);
		$itemSQL->bindValue(':id2', $id);
		$itemRows = $itemSQL->queryAll();

		foreach ($itemRows as $row)
			$items[$row['id']] = $row['title'];

		return $items;
	}

	/**
	 * Updates an existing ItemItem model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id1
	 * @param integer $id2
	 * @return mixed
	 */
	public function actionUpdate($id1, $id2)
	{

		return $this->redirect(['index']);
		// skip everything below

		// $model = $this->findModel($id1, $id2);

		// if ($model->load(Yii::$app->request->post()) && $model->save())
		// 	return $this->redirect(['view', 'id1' => $model->id1, 'id2' => $model->id2]);
		// else
		// 	return $this->render('update', ['model' => $model]);
	}

	/**
	 * Deletes an existing ItemItem model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id1
	 * @param integer $id2
	 * @return mixed
	 */
	public function actionDelete($id1, $id2)
	{
		$this->findModel($id1, $id2)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the ItemItem model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id1
	 * @param integer $id2
	 * @return ItemItem the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id1, $id2)
	{
		if (($model = ItemItem::findOne(['id1' => $id1, 'id2' => $id2])) !== null)
			return $model;
		else
			throw new NotFoundHttpException('The requested page does not exist.');
	}
}
