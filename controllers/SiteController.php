<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SimpleItem;
use app\persona\PersonaIdentity;
use yii\web\NotFoundHttpException;
use yii\web\Request;

class SiteController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','super-secret-login','secure-signin','logout','contact'],
				'rules' => [
					[
						'allow' => false,
						'actions' => ['super-secret-login']
					],
					[
						'allow' => true,
						'actions' => ['index','secure-signin','contact']
					],
					[
						'allow' => true,
						'actions' => ['logout'],
						'roles' => ['@'],
					],
				],
				'denyCallback' => function ($rule, $action) {
					throw new NotFoundHttpException('Unable to resolve the request "'.substr(Yii::$app->request->url, 1).'".');
				}
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			]
		];
	}

	public function actionIndex()
	{
		return $this->render('index', [
			'chartItems' => $this->getChartItems(),
			'popularFoodItems' => $this->getMostPopularItems(0),
			'popularDrinkItems' => $this->getMostPopularItems(1),
			'recentItems' => $this->getMostRecentItems()
		]);
	}

	private function getChartItems()
	{
		// Pre-selected Items
		// return Yii::$app->db->createCommand('
		// 	SELECT id, title, morning, afternoon, night, anytime
		// 	FROM item
		// 	WHERE id IN (1, 2, 3, 4, 5, 6)
		// ')->queryAll();

		// Random Items

		// PostgreSQL
		// return Yii::$app->db->createCommand('
		// 	SELECT id, title, morning, afternoon, night, anytime
		// 	FROM item
		// 	WHERE (morning > 0 OR afternoon > 0 OR night > 0 OR anytime > 0)
		// 		AND LENGTH(title) <= 30
		// 	ORDER BY RANDOM()
		// 	LIMIT 6
		// ')->queryAll();

		// MariaDB
		return Yii::$app->db->createCommand('
			SELECT id, title, morning, afternoon, night, anytime
			FROM item
			WHERE (morning > 0 OR afternoon > 0 OR night > 0 OR anytime > 0)
				AND LENGTH(title) <= 30
			ORDER BY RAND()
			LIMIT 6
		')->queryAll();
	}

	/**
	 * Gets most popular Item models.
	 * @param integer $type: 0 => food, 1 => drink
	 * @return array
	 */
	private function getMostPopularItems($type)
	{
		$mostPopularItems = [];

		$itemSQL = Yii::$app->db->createCommand('
			SELECT id, title, (morning + afternoon + night + anytime) votes
			FROM item
			WHERE type = :type
			ORDER BY votes DESC
			LIMIT 6
		');
		$itemSQL->bindValue(':type', $type);
		$itemRows = $itemSQL->queryAll();

		foreach ($itemRows as $row)
		{
			// PostgreSQL
			// $imageSQL = Yii::$app->db->createCommand('
			// 	SELECT file
			// 	FROM image
			// 	WHERE item_id = :id
			// 	ORDER BY RANDOM()
			// 	LIMIT 5
			// ');

			// MariaDB
			$imageSQL = Yii::$app->db->createCommand('
				SELECT file
				FROM image
				WHERE item_id = :id
				ORDER BY RAND()
				LIMIT 5
			');
			$imageSQL->bindValue(':id', $row['id']);
			$imageRows = $imageSQL->queryAll();

			$mostPopularItems[$row['id']] = new SimpleItem;
			$mostPopularItems[$row['id']]->title = $row['title'];

			if ($imageRows)
				$mostPopularItems[$row['id']]->file = $imageRows[0]['file'];
			else
				$mostPopularItems[$row['id']]->file = null;
		}

		return $mostPopularItems;
	}

	/**
	 * Gets most recent Item models.
	 * @return array
	 */
	private function getMostRecentItems()
	{
		$mostRecentItems = [];

		$itemRows = Yii::$app->db->createCommand('
			SELECT id, title, timestamp
			FROM item
			ORDER BY timestamp DESC
			LIMIT 12
		')->queryAll();

		foreach ($itemRows as $row)
		{
			// PostgreSQL
			// $imageSQL = Yii::$app->db->createCommand('
			// 	SELECT file
			// 	FROM image
			// 	WHERE item_id = :id
			// 	ORDER BY RANDOM()
			// 	LIMIT 5
			// ');

			// MariaDB
			$imageSQL = Yii::$app->db->createCommand('
				SELECT file
				FROM image
				WHERE item_id = :id
				ORDER BY RAND()
				LIMIT 5
			');
			$imageSQL->bindValue(':id', $row['id']);
			$imageRows = $imageSQL->queryAll();

			$mostRecentItems[$row['id']] = new SimpleItem;
			$mostRecentItems[$row['id']]->title = $row['title'];

			if ($imageRows)
				$mostRecentItems[$row['id']]->file = $imageRows[0]['file'];
			else
				$mostRecentItems[$row['id']]->file = null;
		}

		return $mostRecentItems;
	}

	/**
	 * Gets most linked Item models.
	 * @param integer $type: 0 => food, 1 => drink
	 * @return array
	 */
	private function getMostLinkedItems($type)
	{
		$mostLinkedItems = [];
		$itemRows = [];

		$item1SQL = Yii::$app->db->createCommand('
			SELECT id1 id, COUNT(id1) count
			FROM item_item
			WHERE id1 IN (SELECT id FROM item WHERE type = :type)
			GROUP BY id1
			ORDER BY count DESC
			LIMIT 6
		');
		$item1SQL->bindValue(':type', $type);
		$item1Rows = $item1SQL->queryAll();

		$item2SQL = Yii::$app->db->createCommand('
			SELECT id2 id, COUNT(id2) count
			FROM item_item
			WHERE id2 IN (SELECT id FROM item WHERE type = :type)
			GROUP BY id2
			ORDER BY count DESC
			LIMIT 6
		');
		$item2SQL->bindValue(':type', $type);
		$item2Rows = $item2SQL->queryAll();

		foreach ($item1Rows as $row)
			$itemRows[$row['id']] = $row;

		foreach ($item2Rows as $row)
			if (isset($itemRows[$row['id']]))
			{
				if ($row['count'] > $itemRows[$row['id']]['count'])
					$itemRows[$row['id']] = $row;
			}
			else
				$itemRows[$row['id']] = $row;

		// sort array in descending order
		usort($itemRows, function($a, $b) {
			if ($a['count'] === $b['count'])
				return 0;
			return ($a['count'] < $b['count']) ? 1 : -1;
		});

		if (count($itemRows) > 6)
			$itemRows = array_slice($itemRows, 0, 6, true);

		foreach ($itemRows as $row)
		{
			$itemSQL = Yii::$app->db->createCommand('
				SELECT title
				FROM item
				WHERE id = :id
			');
			$itemSQL->bindValue(':id', $row['id']);
			$itemRow = $itemSQL->queryOne();

			// PostgreSQL
			// $imageSQL = Yii::$app->db->createCommand('
			// 	SELECT file
			// 	FROM image
			// 	WHERE item_id = :id
			// 	ORDER BY RANDOM()
			// 	LIMIT 5
			// ');

			// MariaDB
			$imageSQL = Yii::$app->db->createCommand('
				SELECT file
				FROM image
				WHERE item_id = :id
				ORDER BY RAND()
				LIMIT 5
			');
			$imageSQL->bindValue(':id', $row['id']);
			$imageRows = $imageSQL->queryAll();

			$mostLinkedItems[$row['id']] = new SimpleItem;
			$mostLinkedItems[$row['id']]->title = $itemRow['title'];

			if ($imageRows)
				$mostLinkedItems[$row['id']]->file = $imageRows[0]['file'];
			else
				$mostLinkedItems[$row['id']]->file = null;
		}

		return $mostLinkedItems;
	}

	public function actionSuperSecretLogin()
	{
		if (!Yii::$app->user->isGuest)
			return $this->goHome();

		$model = new LoginForm();

		if ($model->load(Yii::$app->request->post()))
		{
			if ($model->login())
				return $this->goHome();
		}
		else
			return $this->render('super-secret-login', ['model' => $model]);
	}

	public function actionSecureSignin()
	{
		if (Yii::$app->request->isPost)
		{
			$identity = new PersonaIdentity;

			if ($identity->authenticate())
			{
				if (Yii::$app->user->login($identity))
					return $this->goHome();
				else
					return $this->render('secure-signin');
			}
			else
				return $this->render('secure-signin');
		}
		else
			return $this->render('secure-signin');
	}

	public function actionLogout()
	{
		Yii::$app->user->logout();
		return $this->goHome();
	}

	public function actionContact()
	{
		return $this->render('contact');
	}
}
