<?php

use yii\db\Schema;
use yii\db\Migration;

class m141203_142704_create_core_tables extends Migration
{
	public function safeUp()
	{
		// PostgreSQL
		// CREATE TABLE item (
		// 	id SERIAL PRIMARY KEY,
		// 	title VARCHAR(70) UNIQUE NOT NULL,
		// 	type SMALLINT CHECK (type IN (0, 1)) NOT NULL,
		// 	morning INT DEFAULT 0,
		// 	afternoon INT DEFAULT 0,
		// 	night INT DEFAULT 0,
		// 	anytime INT DEFAULT 0,
		// 	timestamp TIMESTAMP
		// );
		// CREATE TABLE image (
		// 	id SERIAL PRIMARY KEY,
		// 	item_id INT REFERENCES item ON DELETE CASCADE ON UPDATE RESTRICT NOT NULL,
		// 	file VARCHAR(35) NOT NULL
		// );
		// CREATE TABLE item_item (
		// 	id1 INT REFERENCES item ON DELETE CASCADE ON UPDATE RESTRICT,
		// 	id2 INT REFERENCES item ON DELETE CASCADE ON UPDATE RESTRICT,
		// 	CHECK (id1 < id2),
		// 	PRIMARY KEY (id1, id2)
		// );

		// Query Builder
		$this->createTable('item', [
			'id' => 'pk',
			'title' => 'varchar(70) UNIQUE NOT NULL',
			// 'type' => 'smallint CHECK (type IN (0, 1)) NOT NULL', // PostgreSQL
			'type' => 'smallint NOT NULL', // MariaDB
			'morning' => 'int DEFAULT 0',
			'afternoon' => 'int DEFAULT 0',
			'night' => 'int DEFAULT 0',
			'anytime' => 'int DEFAULT 0',
			'timestamp' => 'timestamp',
			'CHECK (type IN (0, 1))' // MariaDB
		]);
		$this->createTable('image', [
			'id' => 'pk',
			// 'item_id' => 'int REFERENCES item ON DELETE CASCADE ON UPDATE RESTRICT NOT NULL', // PostgreSQL
			'item_id' => 'int NOT NULL', // MariaDB
			'file' => 'varchar(35) NOT NULL',
			'FOREIGN KEY (item_id) REFERENCES item(id) ON DELETE CASCADE ON UPDATE RESTRICT' // MariaDB
		]);
		$this->createTable('item_item', [
			// 'id1' => 'int REFERENCES item ON DELETE CASCADE ON UPDATE RESTRICT', // PostgreSQL
			// 'id2' => 'int REFERENCES item ON DELETE CASCADE ON UPDATE RESTRICT', // PostgreSQL
			'id1' => 'int', // MariaDB
			'id2' => 'int', // MariaDB
			'CHECK (id1 < id2)',
			'PRIMARY KEY (id1, id2)',
			'FOREIGN KEY (id1) REFERENCES item(id) ON DELETE CASCADE ON UPDATE RESTRICT', // MariaDB
			'FOREIGN KEY (id2) REFERENCES item(id) ON DELETE CASCADE ON UPDATE RESTRICT' // MariaDB
		]);
		// $this->addForeignKey('fk_image_item_id', 'image', 'item_id', 'item', 'id', 'CASCADE', 'CASCADE'); // MariaDB
		// $this->addForeignKey('fk_item_item_id1', 'item_item', 'id1', 'item', 'id', 'CASCADE', 'CASCADE'); // MariaDB
		// $this->addForeignKey('fk_item_item_id2', 'item_item', 'id2', 'item', 'id', 'CASCADE', 'CASCADE'); // MariaDB
	}

	public function safeDown()
	{
		// $this->dropForeignKey('fk_image_item_id', 'image'); // MariaDB
		// $this->dropForeignKey('fk_item_item_id1', 'item_item'); // MariaDB
		// $this->dropForeignKey('fk_item_item_id2', 'item_item'); // MariaDB
		$this->dropTable('item_item');
		$this->dropTable('image');
		$this->dropTable('item');
	}
}
